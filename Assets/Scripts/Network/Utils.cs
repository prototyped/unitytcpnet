﻿// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.Text;
using System.Security.Cryptography;

namespace Network
{
    public class Utils
    {
        private static readonly string hexTable = "0123456789abcdef";
        private static readonly DateTime epoch = new DateTime(1970, 1, 1);
        private static readonly DateTime twepoch = new DateTime(2000, 1, 1);

        // Binary data to hex string
        public static string BinaryToHex(byte[] data, int offset = 0)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = offset; i < data.Length; i++)
            {
                byte v = data[i];
                sb.Append(hexTable[v >> 4]);
                sb.Append(hexTable[v & 0x0f]);
            }
            return sb.ToString();
        }

        public static string MD5Sum(byte[] data, int offset, int count)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            var hash = md5.ComputeHash(data, offset, count);
            return BinaryToHex(hash);
        }

        // Swap two values
        public static void Swap<T>(ref T t1, ref T t2)
        {
            T tmp = t1;
            t1 = t2;
            t2 = tmp;
        }

        // Current datetime to unix timestamp
        public static Int64 ToUnixTimestamp(DateTime t)
        {
            var timespan = t.ToUniversalTime().Subtract(epoch);
            return (Int64)Math.Truncate(timespan.TotalSeconds);
        }

        // Unix timestamp to local time
        public static DateTime TimestampToLocalTime(Int64 timestamp)
        {
            return epoch.AddSeconds(timestamp);
        }

        // Current wall clock time in milliseconds
        public static Int64 CurrentTimeMillis()
        {
            return Convert.ToInt64(DateTime.UtcNow.Subtract(epoch).TotalMilliseconds);
        }

        // 自定义时钟
        public static UInt32 iclock()
        {
            var now = Convert.ToInt64(DateTime.UtcNow.Subtract(twepoch).TotalMilliseconds);
            return (UInt32)(now & 0xFFFFFFFF);
        }

        // split `12.34.56.78:8008` to (12.34.56.78, 8008)
        public static void SplitHostPort(string addr, ref string host, ref UInt16 port)
        {
            int idx = addr.LastIndexOf(':');
            if (idx < 0 || idx == 0 || idx == addr.Length)
            {
                throw new ArgumentException(String.Format("invalid host port {0}", addr));
            }
            port = Convert.ToUInt16(addr.Substring(idx + 1));
            host = addr.Substring(0, idx);
        }
    }
}
