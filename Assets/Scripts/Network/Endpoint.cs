// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
    public class Endpoint
    {
        private Queue<Packet> incomming;        // incoming messages to dispatch
        private Queue<Exception> errors;        // 
        private Transport transport;            // tansport object
        private Socket socket;
        private IOBuf iobuf = new IOBuf();
        private Queue<Exception> errque;

        public UInt16 LastSeqNo { get; set; }
        public ICodec Codec { get; set; }

        public Endpoint(ICodec codec)
        {
            Codec = codec;
            errque = new Queue<Exception>(8);
        }

        public void Close()
        {
            if (socket != null)
            {
                socket.Close();
                socket = null;
            }
        }

        public void ConnectTCP(string host, UInt16 port)
        {
            IPAddress[] addrList = Dns.GetHostAddresses(host);
            if (addrList.Length > 0)
            {
                var addr = addrList[0];
                socket = new Socket(addr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(new IPEndPoint(addr, port));
                socket.ReceiveTimeout = (int)Constant.TCPRecvTimeoutSeconds;
                socket.NoDelay = true;
                transport = new Transport(socket, iobuf);
            }
            else
            {
                var msg = String.Format("Cannot resolve host {0}", host);
                throw new InvalidOperationException(msg);
            }
        }

        public void StartRead(Queue<Packet> queue, Queue<Exception> err)
        {
            if (transport != null)
            {
                incomming = queue;
                errors = err;
                transport.StartRead();
            }
            else
            {
                Debug.Log("StartRead: transport is null");
            }
        }

        public void SendPacket(Packet pkt)
        {
            if (!socket.Connected)
            {
                var desc = String.Format("SendPacket: {0} socket already closed", pkt.Command);
                errors.Enqueue(new InvalidOperationException(desc));
                return;
            }
            if (transport == null)
            {
                Debug.LogFormat("SendPacket: transport is null");
                return;
            }
            try
            {
                var data = Codec.Encode(pkt);
                transport.SendBytes(data, 0, data.Length);
            }
            catch (Exception ex)
            {
                errors.Enqueue(ex);
            }
        }

        public void Update()
        {
            if (transport == null)
            {
                return;
            }
            transport.Update();
            while (true)
            {
                Packet pkt = iobuf.ReadPacket(Codec);
                if (pkt != null)
                {
                    incomming.Enqueue(pkt);
                    continue;
                }
                break;
            }
            transport.SwitchErrorQueue(ref errque);
            while (errque.Count > 0)
            {
                errors.Enqueue(errque.Dequeue());
            }
        }
    }
}