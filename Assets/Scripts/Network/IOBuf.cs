﻿// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;


namespace Network
{
    public class IOBuf
    {
        public const int InitialSize = 4096;

        private byte[] buffer_;
        private int writeIndex_ = 0;
        private int readIndex_ = 0;

        public byte[] DataBytes { get { return buffer_; } }
        public int ReadIndex { get { return readIndex_; } }

        public IOBuf(int initSize = InitialSize)
        {
            buffer_ = new byte[initSize];
        }

        public int WritableBytes()
        {
            return buffer_.Length - writeIndex_;
        }

        public int ReadableBytes()
        {
            return writeIndex_ - readIndex_;
        }

        // Write bytes from `data` to buffer
        public void Append(byte[] data, int offset, int size)
        {
            EnsureWritableBytes(size);
            Buffer.BlockCopy(data, offset, buffer_, writeIndex_, size);
            writeIndex_ += size;
        }

        // Read bytes from buffer to `data`
        public int Read(byte[] data, int offset, int size)
        {
            int readable = ReadableBytes();
            int bytesRead = (size > readable ? readable : size);
            Buffer.BlockCopy(buffer_, readIndex_, data, offset, bytesRead);
            readIndex_ += bytesRead;
            return bytesRead;
        }

        public void Retrieve(int size)
        {
            if (size < ReadableBytes())
            {
                readIndex_ += size;
            }
            else
            {
                readIndex_ = 0;
                writeIndex_ = 0;
            }
        }

        void EnsureWritableBytes(int size)
        {
            if (size <= WritableBytes())
            {
                return;
            }
            int readable = ReadableBytes();
            if (WritableBytes() >= size) // move space
            {
                Buffer.BlockCopy(buffer_, readIndex_, buffer_, 0, readable);
            }
            else // make more space
            {
                int newsize = 64;
                while (newsize < size) // alignment
                {
                    newsize *= 2;
                }
                var newbuf = new byte[buffer_.Length + newsize];
                Buffer.BlockCopy(buffer_, readIndex_, newbuf, 0, readable);
                buffer_ = newbuf;
            }
            readIndex_ = 0;
            writeIndex_ = readIndex_ + readable;
        }

        public Packet ReadPacket(ICodec codec)
        {
            int bytes = ReadableBytes();
            if (bytes >= codec.RecvHeaderByteSize)
            {
                var pkt = codec.Decode(DataBytes, ReadIndex, ref bytes);
                Retrieve(bytes);
                return pkt;
            }
            return null;
        }
    }
}
