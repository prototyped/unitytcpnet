﻿// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

namespace Network
{
    public enum Constant
    {
        ProtocolVersion = 10,               // protocol version
        CompressThreshold = 1024,           // 1k
        MaxPacketPayLoad = 16 * 1024,       // 16K

        DefaultHeartBeatSec = 10,           // heartbeat interval
        DefaultReplyTtl = 45,               // RPC timeout
        TCPRecvTimeoutSeconds = 60,         //
        HTTPTimeout = 30,                   //
    }

    public enum ServiceType
    {
        SERVICE_CLIENT = 0x00,          //发送给客户端自己
        SERVICE_LOGIN = 0x02,           //登陆服务
        SERVICE_GATEWAY = 0x03,         //网关服务
        SERVICE_GAME = 0x05,            //单人玩法
        SERVICE_BATTLE = 0x06,          //战斗服务
    };
}
