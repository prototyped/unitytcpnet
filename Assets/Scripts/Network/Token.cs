﻿// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.Text;
using System.Security.Cryptography;
using UnityEngine;

// supersdk token
namespace Network
{
    [Serializable]
    public class OSDKToken
    {
        static public string secretKey = "1!F11I1NDZTax9w";

        public string account_system_id = "";
        public string channel_id = "";
        public string ip = "";
        public string login_sdk_name = "";
        public string osdk_game_id = "";
        public string osdk_user_id = "";
        public string user_id = "";
        public string country = "";
        public long time = 0;
        public string extend = "";
        public string sign = "";

        public OSDKToken()
        {
        }

        public string CalcSign(string secretKey)
        {
            // all keys in alphabetical order
            StringBuilder sb = new StringBuilder();
            sb.Append("account_system_id=").Append(account_system_id);
            sb.Append("&channel_id=").Append(channel_id);
            sb.Append("&country=").Append(country);
            sb.Append("&extend=").Append(extend);
            sb.Append("&ip=").Append(ip);
            sb.Append("&login_sdk_name=").Append(login_sdk_name);
            sb.Append("&osdk_game_id=").Append(osdk_game_id);
            sb.Append("&osdk_user_id=").Append(osdk_user_id);
            sb.Append("&time=").Append(time);
            sb.Append("&user_id=").Append(user_id);
            sb.Append(secretKey);
            var rawbytes = Encoding.UTF8.GetBytes(sb.ToString());
            return Utils.MD5Sum(rawbytes, 0, rawbytes.Length);
        }

        public string ConvertToString()
        {
            sign = CalcSign(secretKey);
            var text = JsonUtility.ToJson(this);
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(text));
        }

        public static OSDKToken Parse(string json)
        {
            var rawbytes = Convert.FromBase64String(json);
            var text = Encoding.UTF8.GetString(rawbytes);
            return JsonUtility.FromJson<OSDKToken>(text);
        }

        public static OSDKToken Create(string account)
        {
            return new OSDKToken
            {
                time = Utils.ToUnixTimestamp(DateTime.Now),
                channel_id = "jjtest",
                account_system_id = "jjtest",
                login_sdk_name = "jjtest",
                osdk_game_id = "121",
                osdk_user_id = String.Format("jjtest_{0}", account),
                user_id = account,
            };
        }
    }
}
