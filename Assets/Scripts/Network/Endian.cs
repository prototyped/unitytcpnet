// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;

namespace Network
{
    public class BigEndian
    {
        public static void WriteU16(byte[] buf, ref int off, UInt16 x)
        {
            buf[off] = (byte)(x >> 8);
            buf[off + 1] = (byte)(x & 0xFF);
            off += 2;
        }

        public static void WriteU32(byte[] buf, ref int off, UInt32 x)
        {
            buf[off] = (byte)(x >> 24);
            buf[off + 1] = (byte)(x >> 16);
            buf[off + 2] = (byte)(x >> 8);
            buf[off + 3] = (byte)(x & 0xFF);
            off += 4;
        }

        public static void WriteU64(byte[] buf, ref int off, UInt32 x)
        {
            buf[off] = (byte)(x >> 56);
            buf[off + 1] = (byte)(x >> 48);
            buf[off + 2] = (byte)(x >> 40);
            buf[off + 3] = (byte)(x >> 32);
            buf[off + 4] = (byte)(x >> 24);
            buf[off + 5] = (byte)(x >> 16);
            buf[off + 6] = (byte)(x >> 8);
            buf[off + 7] = (byte)(x & 0xFF);
            off += 8;
        }

        public static UInt16 ReadU16(byte[] buf, ref int off)
        {
            UInt16 v = (UInt16)((buf[off] << 8) | buf[off + 1]);
            off += 2;
            return v;
        }

        public static UInt32 ReadU32(byte[] buf, ref int off)
        {
            UInt32 v = (UInt32)((buf[off] << 24) | (buf[off + 1] << 16) | (buf[off + 2] << 8) | buf[off + 3]);
            off += 4;
            return v;
        }

        public static UInt64 ReadU64(byte[] buf, ref int off)
        {
            UInt64 v = (UInt64)((buf[off] << 56) | (buf[off + 1] << 48) | (buf[off + 2] << 40) | (buf[off + 3] << 32)
                | (buf[off + 4] << 24) | (buf[off + 5] << 16) | (buf[off + 6] << 8) | buf[off + 3]);
            off += 8;
            return v;
        }
    }

    public class LittleEndian
    {
        public static void WriteU16(byte[] buf, ref int off, UInt16 x)
        {
            buf[off] = (byte)(x & 0xFF);
            buf[off + 1] =  (byte)(x >> 8);
            off += 2;
        }

        public static void WriteU32(byte[] buf, ref int off, UInt32 x)
        {
            buf[off] = (byte)(x & 0xFF);  
            buf[off + 1] = (byte)(x >> 8); 
            buf[off + 2] = (byte)(x >> 16);
            buf[off + 3] = (byte)(x >> 24);
            off += 4;
        }

        public static void WriteU64(byte[] buf, ref int off, UInt64 x)
        {
            buf[off] = (byte)(x & 0xFF); 
            buf[off + 1] = (byte)(x >> 8); 
            buf[off + 2] = (byte)(x >> 16); 
            buf[off + 3] = (byte)(x >> 24); 
            buf[off + 4] = (byte)(x >> 32);
            buf[off + 5] = (byte)(x >> 40);
            buf[off + 6] = (byte)(x >> 48);
            buf[off + 7] = (byte)(x >> 56);
            off += 8;
        }

        public static UInt16 ReadU16(byte[] buf, ref int off)
        {
            UInt16 v = (UInt16)(buf[off] | (buf[off + 1] << 8));
            off += 2;
            return v;
        }

        public static UInt32 ReadU32(byte[] buf, ref int off)
        {
            UInt32 v = (UInt32)(buf[off] | (buf[off + 1] << 8) | (buf[off + 2] << 16) | (buf[off + 3] << 24));
            off += 4;
            return v;
        }

        public static UInt64 ReadU64(byte[] buf, ref int off)
        {
            UInt64 v = (UInt64)(buf[off] | (buf[off + 1] << 8) | (buf[off + 2] << 16) | (buf[off + 3] << 24)
                | (buf[off + 4] << 32) | (buf[off + 5] << 40) | (buf[off + 6] << 48) | (buf[off + 3] << 56));
            off += 8;
            return v;
        }
    }
}