﻿// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Network
{
    // TCP net client
    public class Client : Ticker
    {
        private Dictionary<MsgType, Action<Packet>> handlers;

        private Queue<Packet> incomming;
        private Queue<Exception> errors;

        private Endpoint endpoint;
        public byte[] EncryptKey = new byte[32];
        public UInt64 EncryptIv = 0;

        private Int64 cachedTime = 0;              // cached current time
        public UInt16 LastTCPSeqno { get; set; }
        public UInt16 District { get; set; }


        public Client()
        {
            handlers = new Dictionary<MsgType, Action<Packet>>();
            incomming = new Queue<Packet>(128);
            errors = new Queue<Exception>(8);
            var random = new System.Random((int)DateTime.Now.Ticks);
            EncryptIv = (UInt64)random.Next();
            random.NextBytes(EncryptKey);
        }

        public void Attach(MsgType command, Action<Packet> cb)
        {
            if (handlers.ContainsKey(command))
            {
                handlers[command] += cb;
            }
            else
            {
                handlers[command] = cb;
            }
        }

        public void Detach(MsgType command)
        {
            handlers.Remove(command);
        }

        public void Connect(string address)
        {
            string host = "";
            UInt16 port = 0;
            Utils.SplitHostPort(address, ref host, ref port);
            endpoint = new Endpoint(new TCPV2Codec());
            endpoint.ConnectTCP(host, port);
            endpoint.StartRead(incomming, errors);
        }


        public void Close()
        {
            if (endpoint != null)
            {
                endpoint.Close();
                endpoint = null;
            }
        }

        public void SetCrypter(byte[] decryptKey, UInt64 decryptIv)
        {
            var encrypter = new Salsa20Crypter(EncryptKey, EncryptIv);
            var decrypter = new Salsa20Crypter(decryptKey, decryptIv);
            endpoint.Codec.SetEncrypter(encrypter);
            endpoint.Codec.SetDecrypter(decrypter);
        }

        public void StartHeartbeat()
        {
            Attach(MsgType.SM_HEARTBEAT_STATUS, HandleHeartBeat);
            Schedule((int)Constant.DefaultHeartBeatSec, true, HeartBeat);
        }

        // send packet to specified service
        void SendTo(UInt16 node, MsgType cmd, Google.Protobuf.IMessage msg)
        {
            var pkt = new Packet(node, cmd, msg);
            pkt.SeqNo = LastTCPSeqno++;
            var service = (node >> 8);
            if (service == (int)ServiceType.SERVICE_CLIENT)
            {
                incomming.Enqueue(pkt);
            }
            else
            {
                endpoint.SendPacket(pkt);
            }
            if (cmd != MsgType.CM_HEARTBEAT)
            {
                Debug.LogFormat("Send: {0:X} {1} {2}", node, cmd, msg);
            }
        }

        public void SendTo(ServiceType dest, MsgType cmd, Google.Protobuf.IMessage msg)
        {
            UInt16 node = (UInt16)(((UInt16)dest << 8) | District);
            SendTo(node, cmd, msg);
        }

        public void SendTo(ServiceType dest, UInt16 group, MsgType cmd, Google.Protobuf.IMessage msg)
        {
            UInt16 node = (UInt16)(((UInt16)dest << 8) | group);
            SendTo(node, cmd, msg);
        }

        void HeartBeat()
        {
            var req = new Proto.HeartbeatPing
            {
                Time = (UInt64)cachedTime
            };
            SendTo(ServiceType.SERVICE_GATEWAY, MsgType.CM_HEARTBEAT, req);
            //Debug.LogFormat("HeartBeat: {0}", DateTime.Now.ToString());
        }

        void HandleHeartBeat(Packet pkt)
        {
            //var resp = pkt.Parse<Proto.HeartbeatPong>();
            //Debug.LogFormat("heartbeat response: {0}", resp);
        }

        // cached time
        public Int64 CurrentTime()
        {
            return cachedTime;
        }

        public void Dispatch(Packet pkt)
        {
            Action<Packet> callback;
            var command = (MsgType)pkt.Command;
            if (!handlers.TryGetValue(command, out callback))
            {
                Debug.LogErrorFormat("Message {0} handler not found", command);
                return;
            }
            try
            {
                if (command != MsgType.SM_HEARTBEAT_STATUS)
                {
                    Debug.LogFormat("Recv {0} {1}", command, pkt.Message);
                }
                callback(pkt);
            }
            catch (Exception ex)
            {
                Debug.LogErrorFormat("net exception: {0}", ex);
            }
        }

        void ProcessMessage()
        {
            while (incomming.Count > 0)
            {
                var pkt = incomming.Dequeue();
                Dispatch(pkt);
            }
            if (errors.Count > 0)
            {
                var err = errors.Dequeue();
                Debug.LogErrorFormat("net error: {0}", err);
                Close();
            }
            incomming.Clear();
            errors.Clear();
        }

        // Dispatch messages
        public void Update()
        {
            var now = Utils.CurrentTimeMillis();
            Tick(now);
            ProcessMessage();
            if (endpoint != null)
            {
                endpoint.Update();
            }
        }

    }
}
