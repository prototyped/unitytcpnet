// Auto genereated by `generr.py`, DO NOT EDIT!

namespace Network
{
    public enum Errno
    {
        ErrServerInternalError   = 10011 , //服务器内部错误，请稍后再试
        ErrProtocolIncompatible  = 10012 , //此版本的应用程序已不受支持，请更新。
        ErrServerNotInService    = 10013 , //服务器正在维护中，请稍后再试
        ErrServiceNotAvailable   = 10014 , //服务不可用，请稍后再试
        ErrInvalidArgument       = 10015 , //请求参数错误，请稍后再试
        ErrOperationNotSupported = 10016 , //不支持此操作，请稍后再试
        ErrDatabaseException     = 10017 , //数据库异常，请稍后再试
        ErrServerMaintenance     = 10018 , //抱歉，服务器正在维护，请稍后再试
        ErrBadRequest            = 10019 , //错误的请求，请稍后再试
        ErrRpcTimedout           = 10020 , //RPC请求超时，请稍后再试
        ErrRequestTimedout       = 10021 , //您的请求已超时，请稍后再试
        ErrDuplicateRegister     = 10022 , //重复的节点注册
        ErrDuplicateRequest      = 10023 , //请求已重复，请稍后再试
        ErrAnotherDeviceLogin    = 10031 , //您的账号在另一个设备登陆
        ErrBeenKickOffline       = 10032 , //您已被强制下线，请稍后再试
        ErrLoginTryAgain         = 10033 , //登录游戏失败，请稍后再试
        ErrInvalidSession        = 10034 , //错误的会话，请稍后再试
        ErrInvalidAuthToken      = 10035 , //认证失败，请检查账号
        ErrParseInvalidProto     = 10036 , //错误的消息格式

    }
}