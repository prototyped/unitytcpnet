// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.Security.Cryptography;

namespace Network
{

    public interface ICrypter
    {
        void Encrypt(byte[] src, int srcOffset, int srcSize, byte[] dst, int dstOffset);

        void Decrypt(byte[] src, int srcOffset, int srcSize, byte[] dst, int dstOffset);
    }

    public class Salsa20Crypter : ICrypter
    {
        private byte[] key;
        private byte[] salt = new byte[8];
        private UInt64 nonce;

        public Salsa20Crypter(byte[] key, UInt64 iv)
        {
            nonce = iv;
            UpdateIV();

            // pbkdf2
            var pbkdf2 = new Rfc2898DeriveBytes(key, salt, 4096);
            this.key = pbkdf2.GetBytes(32);
        }

        void UpdateIV()
        {
            int offset = 0;
            LittleEndian.WriteU64(salt, ref offset, nonce);
            nonce++;
        }

        public void Encrypt(byte[] src, int srcOffset, int srcSize, byte[] dst, int dstOffset)
        {
            var salsa20 = new Salsa20();
            var encryptor = salsa20.CreateDecryptor(key, salt);
            encryptor.TransformBlock(src, srcOffset, srcSize, dst, dstOffset);
        }

        public void Decrypt(byte[] src, int srcOffset, int srcSize, byte[] dst, int dstOffset)
        {
            var salsa20 = new Salsa20();
            var encryptor = salsa20.CreateDecryptor(key, salt);
            encryptor.TransformBlock(src, srcOffset, srcSize, dst, dstOffset);
        }
    }
}