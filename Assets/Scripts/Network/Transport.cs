﻿// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;


namespace Network
{
    internal class SocketStateObject
    {
        public byte[] data;
        public int start;
        public int end;
        public IAsyncResult ar;
        public Exception ex;
    }

    // Transport do the dirty work of TCP sending and recieving
    internal class Transport
    {
        private Socket socket;
        private byte[] recvbuf;
        private IOBuf iobuf;
        private Object mutex = new Object();
        private Queue<Exception> errQueue = new Queue<Exception>(8);
        private SocketStateObject recvState = new SocketStateObject();

        public Transport(Socket sock, IOBuf buf)
        {
            socket = sock;
            iobuf = buf;
            recvbuf = new byte[4096]; // init buffer size
        }

        void ProduceError(Exception ex)
        {
            lock (mutex)
            {
                errQueue.Enqueue(ex);
            }
        }

        // Swap background to forground
        public void SwitchErrorQueue(ref Queue<Exception> forground)
        {
            lock (mutex)
            {
                if (errQueue.Count > 0)
                {
                    var tmp = errQueue;
                    errQueue = forground;
                    forground = tmp;
                }
            }
        }

        public void SendBytes(byte[] data, int offset, int size)
        {
            SocketStateObject state = new SocketStateObject
            {
                data = data,
                start = offset,
                end = offset + size,
            };
            socket.BeginSend(state.data, state.start, state.end - state.start, 0,
                WriteCallback, state);
        }

        // This method is executed on a ThreadPool thread, keep this in mind
        void WriteCallback(IAsyncResult ar)
        {
            SocketStateObject state = (SocketStateObject)ar.AsyncState;
            try
            {
                int bytesSent = socket.EndSend(ar);
                state.start += bytesSent;
                if (state.start < state.end) // not finished yet, send bytes remain
                {
                    socket.BeginSend(state.data, state.start, state.end - state.start, 0,
                        new AsyncCallback(WriteCallback), state);
                    return ;
                }

                // write completed
            }
            catch (SocketException ex)
            {
                ProduceError(ex);
            }
        }

        public void StartRead()
        {
            StartRecvBytes(recvState);
        }

        void StartRecvBytes(SocketStateObject state)
        {
            state.ex = null;
            state.data = recvbuf;
            state.start = 0;
            state.end = recvbuf.Length;
            state.ar = socket.BeginReceive(state.data, state.start, state.end - state.start,
                0, null, state);
        }

        void HandleRecv(IAsyncResult ar)
        {
            int bytesRecv = socket.EndReceive(ar);
            if (bytesRecv <= 0) // EOF
            {
                var ex = new EndOfStreamException("socket closed by peer");
                ProduceError(ex);
            }
            else
            {
                iobuf.Append(recvbuf, 0, bytesRecv);
                StartRecvBytes(recvState);
            }
        }

        public void Update()
        {
            IAsyncResult ar = recvState.ar;
            if (ar == null)
            {
                return;
            }

            // This property use Interlocked.CompareExchange internal,
            // see http://referencesource.microsoft.com/#System/net/System/Net/_LazyAsyncResult.cs 
            if (!ar.IsCompleted)
            {
                return;
            }

            try
            {
                HandleRecv(ar);
            }
            catch (SocketException ex)
            {
                ProduceError(ex);
            }
        }
    }
}
