// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.IO;
using UnityEngine;

namespace Network
{
    public interface ICodec
    {
        int SendHeaderByteSize { get; }
        int RecvHeaderByteSize { get; }

        void SetEncrypter(ICrypter v);
        void SetDecrypter(ICrypter v);

        byte[] Encode(Packet pkt);

        Packet Decode(byte[] data, int offset, ref int size);
    }

    // client to server packet header wire layout, 12 bytes
    //       --------------------------------
    // field | len | node | cmd | seq | crc |
    //       --------------------------------
    // bytes |  2  |   2  |  2  |  2  |  4  |
    //       --------------------------------
    //
    // server to client packet header wire layout, 10 bytes
    //       -------------------------------
    // field | len | cmd | flag | seq | ec |
    //       -------------------------------
    // bytes |  2  |  2  |   2  |  2  | 2  |
    //       -------------------------------
    //
    public class TCPV2Codec : ICodec
    {
        public int SendHeaderByteSize { get; set; }
        public int RecvHeaderByteSize { get; set; }

        private ICrypter encrypter;
        private ICrypter decrypter;
        private Crc32Algorithm crc32 = new Crc32Algorithm();
        private byte[] encodeMsgBuf = new byte[1024];
        
        public TCPV2Codec()
        {
            SendHeaderByteSize = 12;
            RecvHeaderByteSize = 10;
        }

        public void SetEncrypter(ICrypter v)
        {
            encrypter = v;
        }

        public void SetDecrypter(ICrypter v)
        {
            decrypter = v;
        }

        UInt32 EncodeHeader(byte[] buf, UInt16 size, Packet pkt)
        {
            int offset = 0;
            BigEndian.WriteU16(buf, ref offset, size);
            BigEndian.WriteU16(buf, ref offset, pkt.Node);
            BigEndian.WriteU16(buf, ref offset, pkt.Command);
            BigEndian.WriteU16(buf, ref offset, pkt.SeqNo);
            
            if (encrypter != null) // in-place encryption
            {
                encrypter.Encrypt(buf, 0, offset, buf, 0);
            }
            return crc32.Compute(buf, 0, offset);
        }

        // `data` is started as header
        void AppendChecksum(byte[] data, int offset, UInt32 crc)
        {
            BigEndian.WriteU32(data, ref offset, crc);
        }


        public byte[] Encode(Packet pkt)
        {
            UInt32 checksum = 0;
            int msgSize = 0;
            if (pkt.Message != null)
            {
                msgSize = pkt.Message.CalculateSize();
            }

            if (msgSize > encodeMsgBuf.Length)
            {
                encodeMsgBuf = new byte[msgSize];
            }
            var data = encodeMsgBuf;
            if (msgSize == 0)
            {
                data = new byte[SendHeaderByteSize];
                checksum = EncodeHeader(data, 0, pkt);
                AppendChecksum(data, SendHeaderByteSize - 4, checksum);
                return data;
            }

            var size = (UInt16)msgSize;
            if (size > (UInt16)Constant.MaxPacketPayLoad)
            {
                var msg = String.Format("{0}: payload size[{1}] out of range", pkt.Command, size);
                throw new ArgumentOutOfRangeException(msg);
            }

            var stream = new MemoryStream(data);
            var output = new Google.Protobuf.CodedOutputStream(stream, msgSize);
            pkt.Message.WriteTo(output);
            output.Flush();
            
            var buffer = new byte[SendHeaderByteSize + size]; // non-resizable
            var crc = EncodeHeader(buffer, size, pkt);
            checksum = crc32.Append(crc, data, 0, size);
            AppendChecksum(buffer, SendHeaderByteSize - 4, checksum);
            Buffer.BlockCopy(data, 0, buffer, SendHeaderByteSize, size);
            return buffer;
        }

        public Packet Decode(byte[] data, int offset, ref int size)
        {
            if (size < RecvHeaderByteSize)
            {
                Debug.LogFormat("packet header not complete yet, {0} != {1}", size, RecvHeaderByteSize);
                return null;
            }
            if (decrypter != null) // in-place decryption
            {
                decrypter.Decrypt(data, offset, RecvHeaderByteSize, data, offset);
            }
            var length = BigEndian.ReadU16(data, ref offset);
            var command = BigEndian.ReadU16(data, ref offset);
            var flag = BigEndian.ReadU16(data, ref offset);
            var seqno = BigEndian.ReadU16(data, ref offset);
            var errno = BigEndian.ReadU16(data, ref offset);            
            if (RecvHeaderByteSize + length > size)
            {
                Debug.LogFormat("packet not complete yet: expect {0}, but got {1}", RecvHeaderByteSize + length, size);
                return null; 
            }

            size = RecvHeaderByteSize + length;  //how many bytes consumed

            var pkt = new Packet();
            pkt.Command = command;
            pkt.SeqNo = seqno;
            pkt.Errno = errno;

            var msg = MsgFactory.ParseFrom((MsgType)pkt.Command, new MemoryStream(data, offset, length));
            if (msg == null)
            {
                Debug.LogErrorFormat("MsgFactor.ParseFrom({0}) failed", pkt.Command);
                return null;
            }
            pkt.Message = msg;
            return pkt;
        }
    }
}
