﻿// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.IO;


namespace Network
{
    // Network transferred stats
    public class Stats
    {
        public UInt32 bytesSent = 0;
        public UInt32 bytesRecv = 0;
        public UInt32 packetSent = 0;
        public UInt32 packetRecv = 0;
    }

    // Packet represents an application message
    public class Packet
    {
        public UInt16 Node { get; set; }
        public UInt16 Command { get; set; }
        public UInt16 SeqNo { get; set; }
        public UInt16 Errno { get; set; }

        public MemoryStream BodyStream { get; set; }
        public Google.Protobuf.IMessage Message { get; set; }

        public Packet()
        {
            Command = 0;
            Errno = 0;
            SeqNo = 0;
        }

        public Packet(UInt16 dest, MsgType cmd, Google.Protobuf.IMessage msg)
        {
            Errno = 0;
            SeqNo = 0;
            Node = dest;
            Command = (UInt16)cmd;
            Message = msg;
        }

        public Packet(UInt16 dest, MsgType cmd, byte[] buf, int offset)
        {
            // keep reference to `buf` object
            BodyStream = new MemoryStream(buf, offset, buf.Length - offset);
            Node = dest;
            Command = (UInt16)cmd;
            Errno = 0;
        }

        public UInt32 GetError()
        {
            return Errno;
        }

        public UInt16 GetBodySize()
        {
            if (BodyStream != null)
            {
                return (UInt16)BodyStream.Length;
            }
            return 0;
        }

        public T Parse<T>() where T : class
        {
            return Message as T;
        }
    }
}
