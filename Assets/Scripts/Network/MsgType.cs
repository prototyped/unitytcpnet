// Auto genereated by `genmsg.py`, DO NOT EDIT!

namespace Network
{
    public enum MsgType
    {
        CM_UDP_HANDSHAKE         = 3001, //握手
        SM_UDP_HANDSHAKE         = 3002, //
        CM_UDP_TERM              = 3003, //断开
        SM_UDP_TERM              = 3004, //
        CM_UDP_PING              = 3005, //心跳
        SM_UDP_PONG              = 3006, //
        SM_KEY_EXCHANGE_NOTIFY   = 3102, //秘钥交换
        SM_DISCONNECT_NOTIFY     = 3110, //断开链接
        CM_HEARTBEAT             = 3111, //心跳
        SM_HEARTBEAT_STATUS      = 3112, //
        SM_ERROR_NOTIFY          = 3113, //
        CM_LOGIN                 = 3201, //登陆获取网关
        SM_LOGIN_STATUS          = 3202, //登陆返回
        CM_ENTER_MAIN_SCREEN     = 3203, //进入主界面
        SM_ENTER_MAIN_SCREEN     = 3204, //
        CM_START_ARENA_MATCH     = 3301, //开始匹配
        SM_START_ARENA_MATCH     = 3302, //
        CM_QUIT_ARENA_MATCH      = 3304, //退出匹配
        SM_QUIT_ARENA_MATCH      = 3305, //
        SM_COMPLETE_MATCH_NOTIFY = 3309, //匹配完成的通知
        CM_JOIN_REGION_MAP       = 4001, //加入战斗场景
        SM_JOIN_REGION_MAP       = 4002, //
        CM_QUIT_REGION           = 4003, //退出场景
        SM_QUIT_REGION           = 4004, //
        SM_ENTER_REGION_NOTIFY   = 4005, //
        SM_QUIT_REGION_NOTIFY    = 4006, //
        CM_MOVE_STEP_REQ         = 4007, //开始移动
        SM_MOVE_STEP_REQ         = 4008, //
        SM_MOVE_STEP_NOTIFY      = 4009, //
        CM_LEFT_JOYSTICK_INPUT   = 4010, //左摇杆输入
        SM_LEFT_JOYSTICK_INPUT   = 4011, //
        SM_LEFT_JOYSTICK_NOTIFY  = 4012, //
        CM_RIGHT_JOYSTICK_INPUT  = 4013, //右摇杆输入
        SM_RIGHT_JOYSTICK_INPUT  = 4014, //
        SM_RIGHT_JOYSTICK_NOTIFY = 4015, //

    }
}
