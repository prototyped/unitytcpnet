// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.IO;
using Ionic.Zlib;

namespace Network
{
    public class Compressor
    {
        private byte[] compresBuf_ = new byte[4096];    // 
        private byte[] decompBuf_ = new byte[4096];     // 

        public Compressor()
        {
        }

        // zlib defalte
        public void Compress(byte[] data, int offset, int count, MemoryStream dst)
        {
            ZlibCodec compressor = new ZlibCodec();
            int rc = compressor.InitializeDeflate(CompressionLevel.Default);
            if (rc != ZlibConstants.Z_OK)
            {
                throw new ArgumentException(String.Format("Compress: deflateInit {0}", rc));
            }
            var buffer = compresBuf_;
            compressor.InputBuffer = data;
            compressor.NextIn = offset;
            compressor.AvailableBytesIn = count;
            compressor.OutputBuffer = buffer;
            for (int pass = 0; pass < 2; pass++)
            {
                FlushType flush = ((pass == 0) ? FlushType.None : FlushType.Finish);
                do
                {
                    compressor.NextOut = 0;
                    compressor.AvailableBytesOut = buffer.Length;
                    rc = compressor.Deflate(flush);
                    if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END)
                    {
                        throw new ArgumentException(String.Format("Compress: deflate {0}", rc));
                    }
                    if (buffer.Length - compressor.AvailableBytesOut > 0)
                    {
                        dst.Write(compressor.OutputBuffer, 0, buffer.Length - compressor.AvailableBytesOut);
                    }
                }
                while (compressor.AvailableBytesIn > 0 || compressor.AvailableBytesOut == 0);
            }
            rc = compressor.EndDeflate();
            if (rc != ZlibConstants.Z_OK)
            {
                throw new ArgumentException(String.Format("Compress: deflateEnd {0}", rc));
            }
        }

        // zlib infalte
        public void UnCompress(byte[] data, int offset, int count, MemoryStream dst)
        {
            ZlibCodec decompressor = new ZlibCodec();
            int rc = decompressor.InitializeInflate();
            if (rc != ZlibConstants.Z_OK)
            {
                throw new ArgumentException(String.Format("UnCompress: inflateInit {0}", rc));
            }

            var buffer = decompBuf_;
            decompressor.InputBuffer = data;
            decompressor.NextIn = offset;
            decompressor.AvailableBytesIn = count;
            decompressor.OutputBuffer = buffer;
            for (int pass = 0; pass < 2; pass++)
            {
                FlushType flush = ((pass == 0) ? FlushType.None : FlushType.Finish);
                do
                {
                    decompressor.NextOut = 0;
                    decompressor.AvailableBytesOut = buffer.Length;
                    rc = decompressor.Inflate(flush);
                    if (rc != ZlibConstants.Z_OK && rc != ZlibConstants.Z_STREAM_END)
                    {
                        throw new ArgumentException(String.Format("UnCompress: inflate {0}", rc));
                    }
                    if (buffer.Length - decompressor.AvailableBytesOut > 0)
                    {
                        dst.Write(decompressor.OutputBuffer, 0, buffer.Length - decompressor.AvailableBytesOut);
                    }
                } while (decompressor.AvailableBytesIn > 0 || decompressor.AvailableBytesOut == 0);
            }
            rc = decompressor.EndInflate();
            if (rc != ZlibConstants.Z_OK)
            {
                throw new ArgumentException(String.Format("UnCompress: inflateEnd {0}", rc));
            }
        }
    }
}