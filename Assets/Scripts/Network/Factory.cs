// Auto genereated by `genmsg.py`, DO NOT EDIT!

using System;
using System.IO;


namespace Network
{
    public class MsgFactory
    {

        public static Google.Protobuf.IMessage New(MsgType command)
        {
            switch (command)
            {
			case MsgType.SM_KEY_EXCHANGE_NOTIFY:
				return new Proto.KeyExchangeNotify();

			case MsgType.SM_DISCONNECT_NOTIFY:
				return new Proto.DisconnectNotify();

			case MsgType.CM_HEARTBEAT:
				return new Proto.HeartbeatPing();

			case MsgType.SM_HEARTBEAT_STATUS:
				return new Proto.HeartbeatPong();

			case MsgType.CM_LOGIN:
				return new Proto.ClientLoginReq();

			case MsgType.SM_LOGIN_STATUS:
				return new Proto.ClientLoginResp();

			case MsgType.CM_ENTER_MAIN_SCREEN:
				return new Proto.EnterMainScreenReq();

			case MsgType.SM_ENTER_MAIN_SCREEN:
				return new Proto.EnterMainScreenRes();

			case MsgType.CM_START_ARENA_MATCH:
				return new Proto.StartMatchGameReq();

			case MsgType.SM_START_ARENA_MATCH:
				return new Proto.StartMatchGameRes();

			case MsgType.CM_QUIT_ARENA_MATCH:
				return new Proto.QuitMatchGameReq();

			case MsgType.SM_QUIT_ARENA_MATCH:
				return new Proto.QuitMatchGameRes();

			case MsgType.SM_COMPLETE_MATCH_NOTIFY:
				return new Proto.MatchCompleteNotify();

			case MsgType.CM_JOIN_REGION_MAP:
				return new Proto.JoinRegionMapReq();

			case MsgType.SM_JOIN_REGION_MAP:
				return new Proto.JoinRegionMapRes();

			case MsgType.CM_QUIT_REGION:
				return new Proto.QuitRegionReq();

			case MsgType.SM_QUIT_REGION:
				return new Proto.QuitRegionRes();

			case MsgType.SM_ENTER_REGION_NOTIFY:
				return new Proto.EnterRegionNotify();

			case MsgType.SM_QUIT_REGION_NOTIFY:
				return new Proto.QuitRegionNotify();

			case MsgType.CM_MOVE_STEP_REQ:
				return new Proto.MoveStepReq();

			case MsgType.SM_MOVE_STEP_NOTIFY:
				return new Proto.MoveStepNotify();

			case MsgType.CM_LEFT_JOYSTICK_INPUT:
				return new Proto.LeftJoystickInputReq();

			case MsgType.SM_LEFT_JOYSTICK_NOTIFY:
				return new Proto.LeftJoystickInputNotify();

			case MsgType.CM_RIGHT_JOYSTICK_INPUT:
				return new Proto.RightJoystickInputReq();

			case MsgType.SM_RIGHT_JOYSTICK_NOTIFY:
				return new Proto.RightJoystickInputNotify();

			
                default:
                    return null;
            }
        }


        public static Google.Protobuf.IMessage ParseFrom(MsgType command, MemoryStream stream)
        {
            switch(command)
            {
			case MsgType.SM_KEY_EXCHANGE_NOTIFY:
				return Proto.KeyExchangeNotify.Parser.ParseFrom(stream);

			case MsgType.SM_DISCONNECT_NOTIFY:
				return Proto.DisconnectNotify.Parser.ParseFrom(stream);

			case MsgType.CM_HEARTBEAT:
				return Proto.HeartbeatPing.Parser.ParseFrom(stream);

			case MsgType.SM_HEARTBEAT_STATUS:
				return Proto.HeartbeatPong.Parser.ParseFrom(stream);

			case MsgType.CM_LOGIN:
				return Proto.ClientLoginReq.Parser.ParseFrom(stream);

			case MsgType.SM_LOGIN_STATUS:
				return Proto.ClientLoginResp.Parser.ParseFrom(stream);

			case MsgType.CM_ENTER_MAIN_SCREEN:
				return Proto.EnterMainScreenReq.Parser.ParseFrom(stream);

			case MsgType.SM_ENTER_MAIN_SCREEN:
				return Proto.EnterMainScreenRes.Parser.ParseFrom(stream);

			case MsgType.CM_START_ARENA_MATCH:
				return Proto.StartMatchGameReq.Parser.ParseFrom(stream);

			case MsgType.SM_START_ARENA_MATCH:
				return Proto.StartMatchGameRes.Parser.ParseFrom(stream);

			case MsgType.CM_QUIT_ARENA_MATCH:
				return Proto.QuitMatchGameReq.Parser.ParseFrom(stream);

			case MsgType.SM_QUIT_ARENA_MATCH:
				return Proto.QuitMatchGameRes.Parser.ParseFrom(stream);

			case MsgType.SM_COMPLETE_MATCH_NOTIFY:
				return Proto.MatchCompleteNotify.Parser.ParseFrom(stream);

			case MsgType.CM_JOIN_REGION_MAP:
				return Proto.JoinRegionMapReq.Parser.ParseFrom(stream);

			case MsgType.SM_JOIN_REGION_MAP:
				return Proto.JoinRegionMapRes.Parser.ParseFrom(stream);

			case MsgType.CM_QUIT_REGION:
				return Proto.QuitRegionReq.Parser.ParseFrom(stream);

			case MsgType.SM_QUIT_REGION:
				return Proto.QuitRegionRes.Parser.ParseFrom(stream);

			case MsgType.SM_ENTER_REGION_NOTIFY:
				return Proto.EnterRegionNotify.Parser.ParseFrom(stream);

			case MsgType.SM_QUIT_REGION_NOTIFY:
				return Proto.QuitRegionNotify.Parser.ParseFrom(stream);

			case MsgType.CM_MOVE_STEP_REQ:
				return Proto.MoveStepReq.Parser.ParseFrom(stream);

			case MsgType.SM_MOVE_STEP_NOTIFY:
				return Proto.MoveStepNotify.Parser.ParseFrom(stream);

			case MsgType.CM_LEFT_JOYSTICK_INPUT:
				return Proto.LeftJoystickInputReq.Parser.ParseFrom(stream);

			case MsgType.SM_LEFT_JOYSTICK_NOTIFY:
				return Proto.LeftJoystickInputNotify.Parser.ParseFrom(stream);

			case MsgType.CM_RIGHT_JOYSTICK_INPUT:
				return Proto.RightJoystickInputReq.Parser.ParseFrom(stream);

			case MsgType.SM_RIGHT_JOYSTICK_NOTIFY:
				return Proto.RightJoystickInputNotify.Parser.ParseFrom(stream);

			
                default:
                    return null;
            }
        }

    }
}
