﻿// Copyright (C) 2017 ichenq@outlook.com. All rights reserved.
// Distributed under the terms and conditions of the Apache License.
// See accompanying files LICENSE.

using System;
using System.Text;
using System.Net;
using System.IO;
using UnityEngine;
using Network;

public class Game : MonoBehaviour
{

    [SerializeField]
    private string hosturl = "http://tps.makingga.me:9527/login/v1";

    private Client client = new Client();
    private string serverAddr;
    private string accessToken;
    private string username;

    // Use this for initialization
    void Start()
    {
        username = PlayerPrefs.GetString("username", "");
        if (username == "")
        {
            username = RandUsername();
            PlayerPrefs.SetString("username", username);
        }
        Debug.LogFormat("start with {0}", username);

        try
        {
            if (LoginToServer())
            {
                ConnectGame();
            }
        }
        catch(Exception ex)
        {
            Debug.LogErrorFormat("{0}", ex);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    //随机帐户名
    string RandUsername()
    {
        var pid = System.Diagnostics.Process.GetCurrentProcess().Id;
        var maybeUnique = string.Format("{0}-{1}-{2}", Dns.GetHostName(), pid, Utils.CurrentTimeMillis());
        var rawbytes = Encoding.UTF8.GetBytes(maybeUnique);
        var checksum = Utils.MD5Sum(rawbytes, 0, rawbytes.Length);
        return string.Format("guest_{0}", checksum.Substring(0, 6));
    }

    //注册消息回调
    void RegisterHandler()
    {
        client.Attach(MsgType.SM_LOGIN_STATUS, HandleLoginStatus);
        client.Attach(MsgType.SM_ENTER_MAIN_SCREEN, HandleEnterMainScreen);
    }

    //登陆服务器，获得网关地址
    bool LoginToServer()
    {
        var token = OSDKToken.Create(username);
        var msg = new Proto.UserAuthReq
        {
            ProtoVer = (UInt32)Constant.ProtocolVersion,
            Token = token.ConvertToString(),
        };
        var msgSize = msg.CalculateSize();
        var data = new MemoryStream(msgSize);
        var output = new Google.Protobuf.CodedOutputStream(data, msgSize);
        msg.WriteTo(output);
        output.Flush();

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(hosturl);
        request.Method = "POST";
        request.Timeout = (int)Constant.HTTPTimeout * 1000;
        request.ContentType = "application/octet-stream";
        request.ContentLength = msgSize;
        var reqStream = request.GetRequestStream();
        data.WriteTo(reqStream);
        reqStream.Close();
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        using (var stream = response.GetResponseStream())
        {
            var reply = Proto.UserAuthResp.Parser.ParseFrom(stream);
            stream.Close();
            if (reply.Status != 0)
            {
                Debug.LogErrorFormat("Login failed: {0}", reply.Status);
                return false;
            }
            serverAddr = reply.ServerAddr;
            accessToken = reply.AccessToken;
            client.District = (UInt16)reply.District;
        }

        return true;
    }

    void ConnectGame()
    {
        client.Connect(serverAddr);

        var req = new Proto.ClientLoginReq()
        {
            AccessToken = accessToken,
            Timestamp = Utils.iclock(),
            Username = username,
        };
        client.SendTo(ServiceType.SERVICE_GATEWAY, MsgType.CM_LOGIN, req);
    }

    //处理登陆返回
    void HandleLoginStatus(Packet pkt)
    {
        var resp = pkt.Parse<Proto.ClientLoginResp>();
        Debug.LogFormat("login response: {0}", resp);
    }

    //进入主界面
    public void HandleEnterMainScreen(Packet pkt)
    {
        var resp = pkt.Parse<Proto.EnterMainScreenRes>();
        Debug.LogFormat("enter game: {0}", resp);
    }
}